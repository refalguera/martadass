
import rlkit
from rlkit.torch import pytorch_util as ptu
from rlkit.data_management.env_replay_buffer import EnvReplayBuffer
from rlkit.envs.wrappers import NormalizedBoxEnv
from rlkit.launchers.launcher_util import setup_logger
from rlkit.samplers.data_collector import MdpPathCollector
from rlkit.torch.sac.policies import TanhGaussianPolicy, MakeDeterministic
from rlkit.torch.sac.sac import SACTrainer
from rlkit.torch.networks import FlattenMlp
from rlkit.torch.torch_rl_algorithm import TorchBatchRLAlgorithm
import torch
from torch.utils import data
from torch.utils.data import DataLoader
import torch.multiprocessing as mp
from torch.autograd import Variable

import argparse
import copy
import numpy as np
import pickle
import uuid

from marta_SAC import MartaWalkEnv

filename = str(uuid.uuid4())

class RL_SAC:

    def __init__(self, variant, data, env_init):

        expl_env = env_init
        eval_env = expl_env
        obs_dim = expl_env.observation_space.low.size
        action_dim = eval_env.action_space.low.size

        eval_policy = data['evaluation/policy']
        eval_path_collector = MdpPathCollector(
            eval_env,
            eval_policy,
        )
        expl_path_collector = MdpPathCollector(
            expl_env,
            data['exploration/policy'],
        )

        replay_buffer = EnvReplayBuffer(
            variant['replay_buffer_size'],
            expl_env,
        )

        self.trainer = SACTrainer(
            env=eval_env,
            policy=data['trainer/policy'],
            qf1=data['trainer/qf1'],
            qf2=data['trainer/qf2'],
            target_qf1=data['trainer/target_qf1'],
            target_qf2=data['trainer/target_qf2'],
            **variant['trainer_kwargs']
        )
        self.algorithm = TorchBatchRLAlgorithm(
            trainer=self.trainer,
            exploration_env=expl_env,
            evaluation_env=eval_env,
            exploration_data_collector=expl_path_collector,
            evaluation_data_collector=eval_path_collector,
            replay_buffer=replay_buffer,
            **variant['algorithm_kwargs']
        )


    def experiment(self):
        self.algorithm.to(ptu.device)
        self.algorithm.train()

    #DASS SELECTION
    def read_expert_samples(self, data_samples):

        for samples in data_samples:
            observations = samples[0]
            actions = samples[1]
            rewards = samples[2]
            next_observations = samples[3]

            #Oberservatio, action, reward and next observation have the same size in 1 sample
            data_size = len(observations)

            if data_size >= 256:
                observations = observations[:256]
                actions = actions[:256]
                rewards = rewards[:256]
                next_observations = next_observations[:256]

                self.trainer.expert_trajectory.push(observations, actions, rewards, next_observations)

def train_policy_rl(args):

    #Load walking policy
    data = torch.load('/home/renata/Marta_neuron/trajectories/policies/Chenatti/params.pkl')
    env_init = data['exploration/env']

    model = RL_SAC(variant, data, env_init)

    if args.training_option == 'DASS':

        # Load the pkl file from the pretrained policy that will be used (READ DASS)
        with open('/home/renata/Marta_neuron/trajectories/Chenatti/expert_trajectory_01.pkl', 'rb') as file:
            data = pickle.load(file)
            model.read_expert_samples(data)

        with open('/home/renata/Marta_neuron/trajectories/Chenatti/expert_trajectory_02.pkl', 'rb') as file2:
            data = pickle.load(file2)
            model.read_expert_samples(data)

        with open('/home/renata/Marta_neuron/trajectories/Chenatti/expert_trajectory_03.pkl', 'rb') as file3:
            data = pickle.load(file3)
            model.read_expert_samples(data)

        model.trainer.supervised = True

    model.experiment()
    env_init.close()
        


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--training_option', type=str, default='DASS',
                        help='SAC OR DASS TRAINING MODE')
    parser.add_argument('--file', type=str, default='', help='PKL FILE')
    parser.add_argument('--gpu', action='store_true')

    args = parser.parse_args()

    variant = dict(
        algorithm="SAC",
        version="normal",
        layer_size=512,
        replay_buffer_size=int(1E6),
        algorithm_kwargs=dict(
            num_epochs=30000000,
            num_eval_steps_per_epoch=5000,
            num_trains_per_train_loop=1000,
            num_expl_steps_per_train_loop=1000,
            min_num_steps_before_training=1000,
            max_path_length=2000,
            batch_size=256,
        ),
        trainer_kwargs=dict(
            discount=0.99,
            soft_target_tau=5e-3,
            target_update_period=1,
            policy_lr=3E-4,
            qf_lr=3E-4,
            reward_scale=1,
            use_automatic_entropy_tuning=True,
        ),
    )

    setup_logger('marta-experiment-DASS', variant=variant)
    ptu.set_gpu_mode(True)  # optionally set the GPU (default=False)
    train_policy_rl(args)
