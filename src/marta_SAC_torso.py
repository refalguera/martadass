""""
SAC Environment Configuration Code.

Implemented by: @refalguera
Last code change: 24/03/2021

"""

from rlkit.core import logger
from pyrep.backend import sim
import os
from os.path import dirname, join, abspath
from shutil import copy
from pyrep import PyRep
from pyrep.robots.humanoids.marta_robot import MartaRobot
from pyrep.objects.shape import Shape
from pyrep.sensors.accelerometer import Accelerometer
from torch.utils.tensorboard import SummaryWriter
import gym
from gym.utils import EzPickle
import numpy as np
import math
import csv
import random

# import sys
# # insert at 1, 0 is the script path (or '' in REPL)
# sys.path.insert(0, '/home/renata/Mestrado/Pesquisa/Marta/Larocs/old_marta_experiments/jaegerlight/python')

# from JaegerLiteAdapter import MartaJaegerLite

# -----------------------------------------------------------------------------------------------
# STEP 1: Define the scene file path --> Marta's Scene is Martabot2.ttt
SCENE_FILE = '/home/renata/Marta/scenes/Martabot2.ttt'
TENSOR_PATH = "/home/renata/Marta/rlkit/data/marta-experiment-DASS/runs/exp17/"
# ------------------------------------------------------------------------------------------------


class MartaWalkEnv(gym.Env, EzPickle):

    """
Init Function:
    - Launch Marta's CoppeliaSim Scene: Martabot2.ttt
    - Instantiate the agent
    - Sets the action and observation space on the Gym Space Box
    """

    def __init__(self):
        EzPickle.__init__(self)
        self.pr = PyRep()

        # Lauch the scene in the Pyrep -> Martabot2.ttt
        self.pr.launch(SCENE_FILE, headless=True,
                       write_coppeliasim_stdout_to_file=True)

        # Inicializate parameters
        self.init = True

        # Reset the environment
        self.reset()

        # Instantiate the agent
        self.agent = MartaRobot()

        # Grabbing the logger path directiory, to place the observed data fille
        # copying the marta_SAC.py to this path
        self.log_dir = logger.get_snapshot_dir()

        # Tensorboard
        self.writer_tensor = SummaryWriter(TENSOR_PATH)

        # Inicializate parameters
        self.step_tracking = []
        self.last_joint_position = []
        self.total_reward = 0
        self.episode = 0
        self.time = 0

        # Sets Control Loop to false
        self.agent.leftArm.set_control_loop_enabled(False)
        self.agent.rightArm.set_control_loop_enabled(False)
        self.agent.leftLeg.set_control_loop_enabled(False)
        self.agent.leftFoot.set_control_loop_enabled(False)
        self.agent.rightLeg.set_control_loop_enabled(False)
        self.agent.rightFoot.set_control_loop_enabled(False)
        self.agent.hip.set_control_loop_enabled(False)

        # Sets if motor is locked when target velocity is zero for all joints.
        self.agent.leftArm.set_motor_locked_at_zero_velocity(True)
        self.agent.rightArm.set_motor_locked_at_zero_velocity(True)
        self.agent.leftLeg.set_motor_locked_at_zero_velocity(True)
        self.agent.leftFoot.set_motor_locked_at_zero_velocity(True)
        self.agent.rightLeg.set_motor_locked_at_zero_velocity(True)
        self.agent.rightFoot.set_motor_locked_at_zero_velocity(True)
        self.agent.hip.set_motor_locked_at_zero_velocity(True)

        # Sets all joints initial positions
        (self.agent.leftArm.initial_joints_positions,
         self.agent.rightArm.initial_joints_positions,
         self.agent.leftLeg.initial_joints_positions,
         self.agent.leftFoot.initial_joints_positions,
         self.agent.rightLeg.initial_joints_positions,
         self.agent.rightFoot.initial_joints_positions,
         self.agent.hip.initial_joints_positions) = self.agent.read_joints_positions()

        self.last_chest_orientation = []
        self.last_chest_position = [0] * 3
        self.chest_orientation_reward_gamma = 0
        self.chest_orientation_reward_alpha = 0

        # Sets the action and observation space on the gym space box
        dim_obs = len(self._make_observation())
        high_obs = np.inf*np.ones([dim_obs])
        self.action_space = gym.spaces.Box(self.agent.low_act_limits, self.agent.high_act_limits)
        self.observation_space = gym.spaces.Box(-high_obs, high_obs)

    # def get_path(self, directory=None):
    #     #Getting files directory
    #     self.directory = directory[:len(directory)-len('/params.pkl')]

# ____________________________________________________________________________________________________

    """
Environment Observation Function:To observe every element in the coppeliasim environment during the observation training phase:
        - Create a state list to add every observed element in the coppeliasim during training
        - Adds the observation elements:
            - Z head
            - Chest inclination (alpha, beta, gamma)
            - V chest (linar speed)
            - W chest (angular speed)
            - Acceleration of IMU 1
            - Acceleration of IMU 2
            - Position of joints
            - Last stock vector
            - Foot force sensors
            - CoM-Based Inertias
        - Returns the observation

        """

    def _make_observation(self):

        # Instantiate Parameters
        state = []  # State

        self.chest_pos_x = []
        self.chest_pos_y = []
        self.chest_pos_z = []

        read_fs = []  # Force sensors (Array of size 8)

        """
            Read the chest z postion, orientation, as also,its linear and angular velocities.
        """

        # Chest reading: position, orientation (alpha, gamma and beta) and the linear and angular velocities
        chest_position = sim.simGetObjectPosition(self.agent.shapes_handle[0], -1)
        self.chest_orientation = sim.simGetObjectOrientation(self.agent.shapes_handle[0], -1)
        chest_lin_vel, chest_ang_vel = sim.simGetObjectVelocity(self.agent.shapes_handle[0])

        # # Chest Position
        state.append(10 * chest_position[2])
        state.append(10 * chest_position[2])

        self.chest_pos_x = chest_position[0]
        self.chest_pos_y = chest_position[1]
        self.chest_pos_z = chest_position[2]

        # Chest Linear
        state.append(chest_lin_vel[0])
        state.append(chest_lin_vel[1])
        state.append(chest_lin_vel[2])
        # Chest Angular
        state.append(chest_ang_vel[0])
        state.append(chest_ang_vel[1])
        state.append(chest_ang_vel[2])
        # Chest Orientation
        state.append(self.chest_orientation[0]) #Alpha
        state.append(self.chest_orientation[1]) #Beta
        state.append(self.chest_orientation[2]) #Gamma


        """
            Read every Marta's force sensors.
        """
        for fs in self.agent.force_sensors_handle:
            read_fs = sim.simReadForceSensor(fs)
            if read_fs != []:
                state.append(read_fs[1][0])
                state.append(read_fs[1][1])
                state.append(read_fs[1][2])
            else:
                state.append(-9999)
                state.append(-9999)
                state.append(-9999)

        """
            Read every Marta's joints positions.
        """
        # if self.steped:
        #     for j in self.last_joint_position:
        #         state.append(j)

        # self.last_joint_position = []  # (Array of size 23)

        for joint_handle in self.agent.joint_handles:
            state.append(sim.simGetJointPosition(joint_handle))
            self.last_joint_position.append(sim.simGetJointPosition(joint_handle))

        # if not self.steped:
        #     for j in self.last_joint_position:
        #         state.append(j)

        """
            Reads the last action taked by the agent or ,if the enviroment just initialized,
          reads the joints initial positions.
        """

        initial_pos = []  # (Array of size 23)

        if self.steped:
            for a in list(self.last_action):
                state.append(a)
        else:
            for joint in self.agent.joint_handles:
                state.append(0)

        """
            Reads the Com-Based Inertia
        """
        self.inertiaMatrix = []  # (Array of size 23*9)

        for shape in self.agent.shapes_handle:
            mass, self.inmatrix = sim.simGetShapeMassAndInertia(shape)
            state.append(mass)
            for value in self.inmatrix:
                state.append(value)
            self.inertiaMatrix.append(self.inmatrix)

        """
            Read both chest accelerometers.
        """
        chest_accel1 = sim.simReadForceSensor(sim.simGetObjectHandle('Marta_chest_accelerometer1_force_sensor'))
        #chest_accel1 = Accelerometer('Marta_chest_accelerometer1').read()
        # chest_accel2 = Accelerometer('Marta_chest_accelerometer2').read()

        state.append(float(chest_accel1[1][0]/0.0001))
        state.append(float(chest_accel1[1][1]/0.0001))
        state.append(float(chest_accel1[1][2]/0.0001))

        # state.append(float(chest_accel2[0]/0.0001))
        # state.append(float(chest_accel2[1]/0.0001))
        # state.append(float(chest_accel2[2]/0.0001))

        # """
        # Gets the number of collisions to identify if both feet and hand collied with the ground.
        # """
        #
        # self.n_colisions = sum([1 if sim.simReadCollision(
        #     collisor) is True else 0 for collisor in self.agent.colid_handles])
        # state.append(self.n_colisions)

        return np.asarray(state).astype('float32')
# _______________________________________________________________________________________________________________________________
    """
        Reset Function:
            - If the scene is running:
                - Save into a file all the actions taken, as well as the calculated trainig rewards
                - Stop the execution of the Scene
            - Inicializate:
                - Number of steps parameter to zero
                - List of the actions
                - List of rewards
            - Start the scene
            - Set the joints to a random postion
        """

    def reset(self):

        # Check if the enviroment is running if it is, stop
        if self.pr.running:
            # Tensorboard
            self.writer_tensor.add_scalar("Total_Reward", self.total_reward, self.episode)
            self.writer_tensor.add_scalar("Chest_Z_Position_Time", self.chest_pos_z, self.episode)
            self.writer_tensor.add_scalar("Chest_Alpha", self.chest_orientation[0], self.episode)
            self.writer_tensor.add_scalar("Chest_Beta", self.chest_orientation[1], self.episode)
            self.writer_tensor.add_scalar("Chest_Gamma", self.chest_orientation[2], self.episode)
           
           
            self.episode += 1
            # # Adds the actions taken and the calculated trainig rewards into a file
            # if self.step_tracking != []:
            #     with open(join(self.log_dir, "data.csv"), "a") as csvfile:
            #         file_is_empty = os.stat(join(self.log_dir, "data.csv")).st_size == 0
            #         headers = ['Episode', 'Step', 'Delta_X', 'Delta_Y', 'Chest_Position_X',
            #                    'Chest_Position_Y', 'Chest Alpha Reward', 'Chest Gamma Reward', 'Reward', 'Inertia']
            #         writer = csv.writer(csvfile)
            #         if file_is_empty:
            #             writer.writerow(headers)
            #         writer.writerows(self.step_tracking)

            self.pr.stop()

        # Inicializate parameters
        self.step_tracking = []
        self.last_joint_position = []
        self.last_chest_position = [0] * 3
        self.chest_orientation_reward_gamma = 0
        self.chest_orientation_reward_alpha = 0
        self.total_reward = 0
        self.time = 0

        # Set step moment to false
        self.steped = False

        # Start the enviroment for the observation
        self.pr.start()

        if self.init:
            print("Successfully Inited")
            self.init = False
            self.episode = 0
            return
        else:
            # Set Marta's joint to the initial position
            for joint in self.agent.joint_handles:
                sim.simSetJointPosition(joint, 0)

            which_leg = random.uniform(0.0,1.0)

            if (which_leg > 0.5):
                pelvis_pitch_handle = self.agent.joint_handles[1]
                knee_pitch_handle = self.agent.joint_handles[5]

            else:
                pelvis_pitch_handle = self.agent.joint_handles[0]
                knee_pitch_handle = self.agent.joint_handles[4]
                

            pelvis_factor = random.uniform(0.0,1.0) * 48 * 0.0174533 + 0.2
            knee_factor = random.uniform(0.0,1.0) * -87 * 0.0174533 - 0.1

            #Zero (default initial position) to max (48 degree)
            sim.simSetJointPosition(pelvis_pitch_handle, pelvis_factor)

            #Zero (not the initial position, but should work) to max (48 degree)
            sim.simSetJointPosition(knee_pitch_handle, knee_factor)

            # Set an initial random position for the joints every reset,
            # in order to increase the different initial states
            action = self.action_space.sample()*0.9

            fixed_joints = ["Marta_leftleg_joint1", "Marta_rightleg_joint1",
                        "Marta_leftleg_joint3", "Marta_rightleg_joint3",
                        "Marta_leftleg_joint5", "Marta_rightleg_joint5",
                        "Marta_leftleg_joint6", "Marta_rightleg_joint6",
                        "Marta_leftfoot_joint1", "Marta_rightfoot_joint1"]
            
            for joint in fixed_joints:
                action[self.agent.joint_order.index(joint)] = 0

                if "Marta_left" in joint:
                    rid = self.agent.joint_order.index("Marta_right" + joint.split("Marta_left")[1])
                    lid = self.agent.joint_order.index(joint)

                    action[rid] = action[lid]

            self.agent.make_action(action)

            # Do a step to make all actions in place
            self.pr.step()

            return self._make_observation()

# ________________________________________________________________________________________________________________

    """
        Step Function:
            - Agent takes an action
            - Observe the enviroment
            - Apply the reward:
                    - Reward inicializate with zero value, then:
                    - (+) Delta X * 500 if DeltaX> 0, if not 1000
                    - (-) 250 * Delta Y

                    - (-) 20% of the reward if chest alpha <0.4 radians (in any direction)

                    OBS:
                      delta x = distance traveled on x
                      chest alpha = chest angle
    """

    def step(self, action):

        # Agent takes an action
        self.agent.make_action(action)

        # Pyrep steping
        self.pr.step()

        # The action taked and the state are saved
        self.last_action = action
        observation = self._make_observation()

        # Inicializate the reward
        reward = 5

        delta_x = (self.chest_pos_x - self.last_chest_position[0])
        delta_y = (self.chest_pos_y - self.last_chest_position[1])

        # + delta x
        if delta_x > 0:
            reward += 500*delta_x
        else:
            reward += 1000*delta_x
        # - delta y
        reward -= 250 * abs(delta_y)

        delta_z = (0.7120 - self.chest_pos_z)

        if delta_z >= 0 and delta_z < 0.2:
            reward += 1000 
        else:
            reward -= 500 * abs(delta_z)

        # Chest Orientation Reward
        #Alpha
        if (abs(self.chest_orientation[0] > 0) and abs(self.chest_orientation[0] < 0.8)):
            reward *= 0.8

        #Gamma
        if (abs(self.chest_orientation[2] > 0) and abs(self.chest_orientation[2] < 0.8)):
            reward *= 0.8

        #Beta
        if (abs(self.chest_orientation[1] > 0) and abs(self.chest_orientation[1] < 0.8)):
            reward *= 2.0

        self.last_chest_position = [self.chest_pos_x, self.chest_pos_y, self.chest_pos_z]
        self.last_chest_orientation = self.chest_orientation

        done = self.chest_pos_z <= 0.56

        self.total_reward += reward
        self.steped = True
        self.time = self.pr.get_simulation_timestep()

        # if not done:
        #     self.step_tracking.append([self.episode, self.time,
        #                                delta_x,
        #                                delta_y,
        #                                self.chest_pos_x, self.chest_pos_y,
        #                                self.chest_orientation_reward_alpha,
        #                                self.chest_orientation_reward_gamma, reward, self.inertiaMatrix])

        return observation, reward, done, {}

    def render(self):
        pass

    def close(self):
        self.writer_tensor.close()
        self.pr.stop()
        self.pr.shutdown()
        print('Success!')


# ---------------------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    env = MartaWalkEnv()
    agent = MartaRobot()
    on = True

    fixed_joints = ["Marta_leftleg_joint1", "Marta_rightleg_joint1",
                "Marta_leftleg_joint3", "Marta_rightleg_joint3",
                "Marta_leftleg_joint5", "Marta_rightleg_joint5",
                "Marta_leftleg_joint6", "Marta_rightleg_joint6",
                "Marta_leftfoot_joint1", "Marta_rightfoot_joint1"]
    while on:
        env.reset()
        for _ in range(10000):       
            action = env.action_space.sample()*0.9

            for joint in fixed_joints:
                action[agent.joint_order.index(joint)] = 0

                if "Marta_left" in joint:
                    rid = agent.joint_order.index("Marta_right" + joint.split("Marta_left")[1])
                    lid = agent.joint_order.index(joint)

                    action[rid] = action[lid]

            observation, reward, done, _ = env.step(action)

            if done:
                break

        if _ == 10000:
            on = False

    print('Done!')

    env.close()
