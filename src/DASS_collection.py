from os import write
from rlkit.samplers.rollout_functions import rollout
from rlkit.torch.pytorch_util import set_gpu_mode
import argparse
import torch
import uuid
from rlkit.core import logger
from pyrep.backend import sim

from functools import partial

import numpy as np
import copy
import csv
import os
import pickle

create_rollout_function = partial


from marta_SAC import MartaWalkEnv

from torch.utils.tensorboard import SummaryWriter

filename = str(uuid.uuid4())

class ExecutePolicy():

    def rollout(self, 
            env,
            agent,
            max_path_length=300,
            render=False,
            render_kwargs=None,
            preprocess_obs_for_policy_fn=None,
            get_action_kwargs=None,
            return_dict_obs=False,
            full_o_postprocess_func=None,
            reset_callback=None,
    ):
        if render_kwargs is None:
            render_kwargs = {}
        if get_action_kwargs is None:
            get_action_kwargs = {}
        if preprocess_obs_for_policy_fn is None:
            preprocess_obs_for_policy_fn = lambda x: x

        raw_obs = []
        raw_next_obs = []
        observations = []
        actions = []
        rewards = []
        terminals = []
        agent_infos = []
        env_infos = []
        next_observations = []

        pos = []
        orientation= []
        
        path_length = 0

        total_samples = 0
        max_samples = 50

        agent.reset()
        o = env.reset()

        if reset_callback:
            reset_callback(env, agent, o)
        if render:
            env.render(**render_kwargs)

        while total_samples < max_samples:
            while path_length < max_path_length:
                raw_obs.append(o)
                o_for_agent = preprocess_obs_for_policy_fn(o)
                a, agent_info = agent.get_action(o_for_agent, **get_action_kwargs)
                
                pos.append(env.chest_pos_z)
                orientation.append(env.chest_orientation)

                if full_o_postprocess_func:
                    full_o_postprocess_func(env, agent, o)

                next_o, r, d, env_info = env.step(copy.deepcopy(a))

                observations.append(o)
                rewards.append(r)
                terminals.append(d)
                actions.append(a)
                next_observations.append(next_o)
                raw_next_obs.append(next_o)
                agent_infos.append(agent_info)
                env_infos.append(env_info)
                path_length += 1

                if d:
                    break
                o = next_o
            
            actions = np.array(actions)
            if len(actions.shape) == 1:
                actions = np.expand_dims(actions, 1)
            observations = np.array(observations)
            next_observations = np.array(next_observations)
            if return_dict_obs:
                observations = raw_obs
                next_observations = raw_next_obs
            rewards = np.array(rewards)
            if len(rewards.shape) == 1:
                rewards = rewards.reshape(-1, 1)


            self.expert_trajectory.append([observations, actions, rewards, next_observations, terminals])

            total_samples += 1
           
            agent.reset()
            o = env.reset()
            raw_obs = []
            raw_next_obs = []
            observations = []
            actions = []
            rewards = []
            terminals = []
            agent_infos = []
            env_infos = []
            next_observations = []
            path_length = 0
            pos = []
            orientation= []

    def simulate_policy(self, args):

        data = torch.load(args.file)
        policy = data['evaluation/policy']
        env = data['evaluation/env']
        
        self.reward = []
        self.position = []
        self.orientation = []

        self.expert_trajectory = []

        print("Policy loaded")
        if args.gpu:
            set_gpu_mode(True)
            policy.cuda()
       
        self.rollout(env,policy,max_path_length=args.H,render=True)
        with open(args.pkl, 'wb') as f:
             pickle.dump(self.expert_trajectory, f)

    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str,
                        help='path to the snapshot file')
    parser.add_argument('--pkl', type=str,
                         help='path to the snapshot file')
    parser.add_argument('--H', type=int, default=300,
                        help='Max length of rollout')
    parser.add_argument('--gpu', action='store_true')
    args = parser.parse_args()

    policy_new = ExecutePolicy()
    policy_new.simulate_policy(args)
