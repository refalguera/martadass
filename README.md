**Deep Reinforcement Learning Application in Humanoid Robots**

Master's Thesis from Renata Falguera. 

Abstract: Rapid advances in robotics, as well as the constant growth of its use in service applications, are driving the field in search of robots that can operate in different environments designed for humans. In particular, humanoid robots are very useful for various service tasks as they can navigate and interact with environments similar to humans. However, bipedal locomotion has proved to be a challenge in theory and in practice due to the difficulty in developing the static and dynamic stability of walking and the complex motor coordination for a sufficient range of motion. Concomitantly, Deep Reinforcement Learning (DRL) methods are becoming a prominent proposal to solve challenging control problems in robotics due to their ability to work in continuous and modelless processes. However, it is difficult to predict the outcome changes made to the reward function, the policy architecture and the set of tasks being trained. Therefore, we propose an iterative policy framework that combines DRL and Stochastic State of Deterministic Action (DASS) samples to allow the reward function to be fully redefined in each successive design iteration, while limiting deviation from the previous iteration. The DASS samples represent deterministic policy action-state pairs sampled from the states visited by a trained stochastic policy. Through the use of a Soft Actor-Critic (SAC) method, policies are trained, which mixes the RL-based policy with DASS-defined updates. The effectiveness of this iterative policy approach will be demonstrated in the simulation of the humanoid robot model Marta, developed by the working groups GASI (Unesp) and LaRoCs (Unicamp).

Full text: https://bdtd.ibict.br/vufind/Record/UNICAMP-30_7da0a92f656ce3fbb0200e2d413e1fc0

Project was developed with:
- Python 3.7.
- PyRep API: toolkit for robot learning research, built on top of CoppeliaSim (previously called V-REP). 
- Rlkit: Reinforcement learning framework and algorithms implemented in PyTorch.
- CoppeliaSim Simulator.

Codes:
    
- in /src: 
    - marta_SAC_*.py: configuration of the Reinforcement Learning environment for the policy training. There it was defined what would be observed in the environment by the robot, the robot action and the reward that would be received based on the action executed. 
    - run_*.py: Policy training algorithm. Configuration of parameters for the deep reinforcement learning algorithm.

- in rlkit/rlkit/torch/sac/sac.py: Rlkit Soft Actor Critic (Deep Reinforcement Learning) algorithm restructured based on the project purposes. There the model was restructured to fit the DASS approach.

- in https://gitlab.com/LaRoCS/PyRep/-/tree/renata/pyrep/robots/humanoids: Code developed for the configuration of the structure of robot. Code only available for laboratory members.

